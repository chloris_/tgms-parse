require "spec"
require "../src/tgms_file_parser.cr"

DELTA_TIME        = 0.001
DELTA_MASS        =  0.01
TOLERANCE_CURRENT =  0.01 # [%]

def check_relative_close(value : Float64, reference : Float64,
                         tolerance : Float64)
  # Discrepancy in [%]
  discrepancy = (value - reference) / reference * 100.0
  discrepancy.abs.should be < tolerance
end

describe "TGMSFileParser" do
  it "extracts data from file \"spec/test_data/short.asc\" correctly" do
    masses = [1.0, 2.0, 3.0]
    currents_1 = [1.335948e-009, 6.376695e-011, 1.426444e-011]
    currents_2 = [1.342016e-009, 6.253027e-011, 1.360046e-011]
    currents_3 = [1.342463e-009, 6.241083e-011, 1.200700e-011]

    parser = TGMSFileParser.new("spec/test_data/short.asc")
    result = parser.parse_file

    result.should be_a(MSData)
    if result.is_a?(MSData)
      result.source_file.should eq "Mia TG vz.142 p4K.qmp"
      result.time_points.size.should eq 3

      tp = result.time_points[0]
      tp.relative_time.should be_close(0.0, DELTA_TIME)
      tp.currents.each_with_index do |c, i|
        c.mass.should be_close(masses[i], DELTA_MASS)
        check_relative_close(c.ion_current, currents_1[i], TOLERANCE_CURRENT)
      end

      tp = result.time_points[1]
      tp.relative_time.should be_close(9.149/60.0, DELTA_TIME)
      tp.currents.each_with_index do |c, i|
        c.mass.should be_close(masses[i], DELTA_MASS)
        check_relative_close(c.ion_current, currents_2[i], TOLERANCE_CURRENT)
      end

      tp = result.time_points[2]
      tp.relative_time.should be_close(18.293/60.0, DELTA_TIME)
      tp.currents.each_with_index do |c, i|
        c.mass.should be_close(masses[i], DELTA_MASS)
        check_relative_close(c.ion_current, currents_3[i], TOLERANCE_CURRENT)
      end
    end
  end
end
