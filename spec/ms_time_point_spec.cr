require "spec"
require "../src/ms_time_point.cr"

describe "MSTimePoint" do
  it "initializes correctly" do
    time = 0.014
    dp = MSTimePoint.new(time)

    dp.relative_time.should eq time
  end

  describe "IonCurrent collection" do
    time = 0.014
    ic1 = IonCurrent.new(1.0, 1e-5)
    ic2 = IonCurrent.new(2.0, 5.7e-7)
    ic3 = IonCurrent.new(10.0, 1.37e-9)

    it "sorts currents" do
      dp = MSTimePoint.new(time)
      dp.currents << (ic1)
      dp.currents << (ic3)
      dp.currents << (ic2)

      dp.currents[0].mass.should eq 1.0
      dp.currents[1].mass.should eq 10.0
      dp.currents[2].mass.should eq 2.0

      dp.sort_currents

      dp.currents[0].mass.should eq 1.0
      dp.currents[1].mass.should eq 2.0
      dp.currents[2].mass.should eq 10.0
    end
  end
end
