require "spec"
require "../src/ion_current.cr"

describe "IonCurrent" do
  it "initializes correctly" do
    mass = 2.0
    current = 1.339e-9

    ic = IonCurrent.new(mass, current)
    ic.mass.should eq mass
    ic.ion_current.should eq current
  end

  it "compares correctly" do
    ic1 = IonCurrent.new(1.0, 1e-9)
    ic2 = IonCurrent.new(6.0, 4e-12)
    ic3 = IonCurrent.new(6.0, 8e-8)

    # Equal everything
    result = ic2 <=> ic2
    result.should eq 0

    # Equal masses only
    result = ic2 <=> ic3
    result.should eq 0

    # Different masses
    result = ic1 <=> ic3
    result.should eq -1
  end
end
