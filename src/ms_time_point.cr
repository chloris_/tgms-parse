require "./ion_current.cr"

# Contains ion currents of all monitored masses at the given time point.
class MSTimePoint
  # Time in minutes since the beginning of the measurement
  getter relative_time
  # Collection of mass, ion current pairs
  getter currents

  # Initializes a new object with relative data. Mass-ion current pairs have
  # to be added to the collection afterwards.
  def initialize(relative_time : Float64)
    if relative_time < 0.0
      raise "Relative time is negative!"
    end
    @relative_time = relative_time
    @currents = [] of IonCurrent
  end

  # Compares two `MSTimePoint` objects by their relative times.
  def <=>(other : MSTimePoint)
    @relative_time <=> other.relative_time
  end

  # Sorts `IonCurrent` objects based on their own comparison method.
  def sort_currents
    @currents.sort!
  end

  # Prints contained data to standard output for debugging purposes.
  def debug_print
    puts(self)
    printf("Relative time: %.3f min\n", @relative_time)
    @currents.each do |ic|
      printf("%5.1f %15.6e\n", ic.mass, ic.ion_current)
    end
  end
end
