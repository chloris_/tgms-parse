require "./ms_data.cr"

# Exports parsed TG-MS data and generates Gnuplot scripts to plot
# exported data.
class DataExporter
  # Format strings
  private TIME_FORMAT        = "%8.3f"
  private MASS_FORMAT        = "%6.2f"
  private ION_CURRENT_FORMAT = "%14.6e"

  # Creates a new object that will source TG-MS data from *ms_data*.
  def initialize(ms_data : MSData)
    @ms_data = ms_data
  end

  # Exports data to file located at given *path*.
  #
  # Returns `true` if exporting was successful or `false` in case of errors.
  def export_to_file(path : String)
    begin
      File.open(path, "w") do |file|
        @ms_data.time_points.each do |tp|
          # Print relative time
          file.printf(TIME_FORMAT, tp.relative_time)
          # Print all (mass, ion current) data pairs
          tp.currents.each do |c|
            file.printf(MASS_FORMAT, c.mass)
            file.printf(ION_CURRENT_FORMAT, c.ion_current)
          end
          # End line
          file.printf("\n")
        end
      end
    rescue exception
      STDERR.puts("Error while exporting file '#{path}':")
      STDERR.puts(exception.message)
      return false
    end
    # No errors
    return true
  end

  # Writes a Gnuplot 2D plot script to the file with given *path*.
  #
  # Path to the exported data file must be provided by *data_path*. Returns
  # `true` if the file was written successfuly or `false` otherwise.
  def write_gnuplot_2d_plot_script(path : String, data_path : String)
    begin
      File.open(path, "w") do |file|
        if @ms_data.time_points.size < 1
          STDERR.puts("No time points to write Gnuplot script!")
          return false
        end
        tp = @ms_data.time_points[0]

        first = true
        first_begin = "plot \"#{data_path}\" u "
        others_begin = "     \"#{data_path}\" u "

        tp.currents.each_with_index do |c, i|
          if first
            first = false
            file.puts("set xlabel \"Time [min]\"")
            file.puts("set ylabel \"Ion Current [A]\"")
            file.print(first_begin)
          else
            file.print(",\\\n")
            file.print(others_begin)
          end
          data_column = 2*i + 3
          title = sprintf("%.2f", c.mass)
          file.print("1:#{data_column} w l title \"#{title}\"")
        end
      end
    rescue exception
      STDERR.puts("Error while writing Gnuplot script '#{path}':")
      STDERR.puts(exception.message)
      return false
    end
    # No errors
    return true
  end

  # Writes a Gnuplot 3D plot script to the file with given *path*.
  #
  # Path to the exported data file must be provided by *data_path*. Returns
  # `true` if the file was written successfuly or `false` otherwise.
  def write_gnuplot_3d_plot_script(path : String, data_path : String)
    begin
      File.open(path, "w") do |file|
        if @ms_data.time_points.size < 1
          STDERR.puts("No time points to write Gnuplot script!")
          return false
        end
        tp = @ms_data.time_points[0]

        first = true
        first_begin = "splot \"#{data_path}\" u "
        others_begin = "      \"#{data_path}\" u "

        # Iterate through currents in reverse direction. Otherwise Gnuplot
        # plots the fence plot so that lines in the background are visible
        # through lines in the foreground.
        i = tp.currents.size
        tp.currents.reverse_each do |c|
          i -= 1
          if first
            first = false
            file.puts("set ylabel \"Mass [amu]\" rotate parallel")
            file.puts("set xlabel \"Time [min]\" rotate parallel")
            file.puts("set zlabel \"Ion Current [A]\" rotate parallel " \
                      "offset -5")
            file.print(first_begin)
          else
            file.print(",\\\n")
            file.print(others_begin)
          end
          ic_column = 2*i + 3
          mass_column = 2*i + 2
          title = sprintf("%.2f", c.mass)
          # Columns: time (axis x), mass (axis y), z_base, z_base,
          # ion current (axis z)
          file.print("1:#{mass_column}:(0):(0):#{ic_column} with " \
                     "zerrorfill title \"#{title}\"")
        end
      end
    rescue exception
      STDERR.puts("Error while writing Gnuplot script '#{path}':")
      STDERR.puts(exception.message)
      return false
    end
    # No errors
    return true
  end
end # End class
