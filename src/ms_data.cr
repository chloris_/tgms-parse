require "./ms_time_point.cr"

# Contains all relevant data read from TG-MS data file.
class MSData
  # Sourcefile attribute from the TG-MS data file
  property source_file
  # Collection of (mass, ion curent) points with different relative
  # measurement times
  getter time_points

  # Initializes an empty `MSData` object.
  def initialize
    @source_file = ""
    @time_points = [] of MSTimePoint
  end
end
