require "option_parser"
require "./tgms_file_parser.cr"
require "./data_exporter.cr"

# Version information
# Version of the application
VERSION = "1.0.0"
# Date of release of this version
DATE = "2020-09-09"

# File name constants
# File name of exported file
EXPORTED_FILE_NAME = "tgms_export.asc"
# File name of Gnuplot 2D plot script
GNUPLOT_2D_FILE_NAME = "plot_2d.gnuplot"
# File name of Gnuplot 3D plot script
GNUPLOT_3D_FILE_NAME = "plot_3d.gnuplot"

# Parse command line arguments
OptionParser.parse do |parser|
  parser.on("-h", "--help", "Show help") do
    show_help
    exit
  end
  parser.on("-v", "--version", "Show version") do
    show_version
    exit
  end
  parser.invalid_option do |flag|
    STDERR.puts("Option '#{flag}' not recognized.")
    exit(1)
  end
end

# Only name of the data file to parse should remain in ARGV at this point
if ARGV.size == 0
  STDERR.puts("Missing the name of TG-MS data file.")
elsif ARGV.size > 1
  STDERR.puts("Too many arguments provided (#{ARGV.size}). " \
              "The only required argument is the name of the TG-MS data file.")
else
  show_version
  puts("")
  file_name = ARGV[0]
  execute_parse(file_name)
end

# Prints program version and date.
def show_version
  puts("tgms-parse #{VERSION} (#{DATE})")
end

# Prints help on usage of this application.
def show_help
  puts ("Usage: tgms-parse TG-MS_DATA_FILE")
  puts ("\nParses TG-MG_DATA_FILE, exports the data to " \
        "'#{EXPORTED_FILE_NAME}', and writes 2D and 3D Gnuplot " \
        "plot scripts.")
end

# Parses TG-MS data from *data_file*, exports it to `EXPORTED_FILE_NAME`, and
# writes 2D and 3D Gnuplot plot scripts.
def execute_parse(data_file : String)
  parser = TGMSFileParser.new(data_file)
  puts("Parsing file '#{data_file}'.")
  parse_result = parser.parse_file
  if parse_result.nil?
    STDERR.puts("Parsing failed.")
    exit(1)
  end

  exporter = DataExporter.new(parse_result)
  puts("Exporting data to file '#{EXPORTED_FILE_NAME}'.")
  export_result = exporter.export_to_file(EXPORTED_FILE_NAME)
  unless export_result
    STDERR.puts("Exporting failed.")
    exit(1)
  end

  puts("Writing Gnuplot 2D plot script '#{GNUPLOT_2D_FILE_NAME}'.")
  g2d_export_result = exporter.write_gnuplot_2d_plot_script(
    GNUPLOT_2D_FILE_NAME,
    EXPORTED_FILE_NAME)
  unless g2d_export_result
    STDERR.puts("Writing script failed.")
    exit(1)
  end

  puts("Writing Gnuplot 3D plot script '#{GNUPLOT_3D_FILE_NAME}'.")
  g3d_export_result = exporter.write_gnuplot_3d_plot_script(
    GNUPLOT_3D_FILE_NAME,
    EXPORTED_FILE_NAME)
  unless g3d_export_result
    STDERR.puts("Writing script failed.")
    exit(1)
  end
end
