require "./ms_data.cr"

# Parses TG-MS data file and stores data in a MSData object.
class TGMSFileParser
  # Path to the TG-MS data file
  getter path

  # Shared Regex objects
  @@regex_sourcefile = /^\s*Sourcefile\s+(.+)\s*$/
  @@regex_start_time = /^\s*Start Time\s+(.+)\s*$/
  @@regex_task_name = /^\s*Task\s+Name\s+Scan\s*$/
  @@regex_data_header = /^\s*Mass\s+\[amu\]\s+Ion Current\s+\[A\]\s*$/
  @@regex_data_line = /^\s*(\S+)\s+(\S+)\s*$/

  # Constants
  # Maximum number of non-empty skipped lines before parsing terminates
  private MAX_SKIPPED_LINES = 10
  # Format string to parse time in TG-MS data file
  private INPUT_TIME_FORMAT = "%-m.%-d.%Y %T.%L"

  # Private instance variables used for parsing
  # Parse stage
  @stage = ParseStage::Sourcefile
  # Content of the latest line read
  @line = ""
  # Number of lines read
  @line_number = 0
  # Number of skipped non-empty lines that contained no data to be parsed
  @skipped_lines = 0
  # Measurement start time
  @measurement_start_time : Time = Time::UNIX_EPOCH
  # Start time of the current scan. Used with measurement start time to
  # calculate relative time of the scan.
  @scan_start_time : Time = Time::UNIX_EPOCH
  # Object to accumulate parsed data
  @ms_data = MSData.new
  # Current MS time point to collect data into
  @ms_time_point = MSTimePoint.new(0.0)

  # Intializes a new TGMSFileParser.
  #
  # Parameters:
  # * *path*: path to the TG-MS data file
  def initialize(path : String)
    @path = path
  end

  # Parses the TG-MS data file and returns a `MSData` object.
  # Includes file existence and readability check.
  #
  # Returns `MSData` object if parse was successful or `nil` otherwise.
  def parse_file
    # Check existence and readability of the data file, return nil in case
    # of failure
    unless File.exists?(@path)
      STDERR.puts("File '#{@path}' does not exist!")
      return nil
    end
    unless File.readable?(@path)
      STDERR.puts("File '#{@path}' is not readable!")
    end

    # Reinitialize variables
    @stage = ParseStage::Sourcefile
    @line_number = 0
    @skipped_lines = 0
    @ms_data = MSData.new

    # Open the file and read it one line at the time
    begin
      File.open(@path) do |file|
        file.each_line do |line|
          @line_number += 1
          @line = line.strip
          if @line.empty?
            next
          end

          case @stage
          when ParseStage::Sourcefile
            parse_sourcefile
          when ParseStage::MeasurementStartTime
            parse_measurement_start_time
          when ParseStage::TaskName
            parse_task_name
          when ParseStage::ScanStartTime
            parse_scan_start_time
          when ParseStage::DataHeader
            parse_data_header
          when ParseStage::DataLine
            parse_data_line
          when ParseStage::Abort
            puts("Aborting parse process due to errors.")
            return nil
          else
            raise "Unknown parse stage: #{@stage}"
          end

          if @stage == ParseStage::EndDataSeries
            end_data_series
            @stage = ParseStage::TaskName
            # Run TaskName handler on the current line
            parse_task_name
          end

          if @skipped_lines > MAX_SKIPPED_LINES
            puts("More than #{MAX_SKIPPED_LINES} lines contained no data " \
                 "to be parsed. Exiting.")
            return nil
          end
        end

        # Commit last data series to the collection if needed
        end_last_data_series
      end
    rescue exception
      STDERR.puts("Exception caught while reading file '#{@path}':")
      STDERR.puts(exception.message)
    end

    # Return parsed data only if it contains data
    if @ms_data.time_points.size > 0
      return @ms_data
    else
      return nil
    end
  end

  # Executes parse stage Sourcefile.
  private def parse_sourcefile
    match = @@regex_sourcefile.match(@line)
    if match.nil?
      @skipped_lines += 1
    else
      @skipped_lines = 0
      @ms_data.source_file = match[1]
      @stage = ParseStage::MeasurementStartTime
    end
  end

  # Executes parse stage MeasurementStartTime
  private def parse_measurement_start_time
    match = @@regex_start_time.match(@line)
    if match.nil?
      @skipped_lines += 1
    else
      @skipped_lines = 0
      time_string = match[1]
      begin
        time = Time.parse_utc(time_string, INPUT_TIME_FORMAT)
        @measurement_start_time = time
        @stage = ParseStage::TaskName
      rescue exception
        STDERR.puts("Error when parsing time:")
        STDERR.puts(exception.message)
        @stage = ParseStage::Abort
      end
    end
  end

  # Executes parse stage TaskName
  private def parse_task_name
    if @line =~ @@regex_task_name
      @skipped_lines = 0
      @stage = ParseStage::ScanStartTime
    else
      @skipped_lines += 1
    end
  end

  # Executes parse stage ScanStartTime
  private def parse_scan_start_time
    match = @@regex_start_time.match(@line)
    if match.nil?
      @skipped_lines += 1
    else
      @skipped_lines = 0
      time_string = match[1]
      begin
        time = Time.parse_utc(time_string, INPUT_TIME_FORMAT)
        @scan_start_time = time
        @stage = ParseStage::DataHeader
      rescue exception
        STDERR.puts("Error when parsing time:")
        STDERR.puts(exception.message)
        @stage = ParseStage::Abort
      end
    end
  end

  # Executes parse stage DataHeader
  private def parse_data_header
    if @line =~ @@regex_data_header
      @skipped_lines = 0
      @stage = ParseStage::DataLine
      prepare_new_ms_time_point
    else
      @skipped_lines += 1
    end
  end

  # Prepares a new `MSTimePoint` object to collect parsed data into.
  private def prepare_new_ms_time_point
    relative_time = @scan_start_time - @measurement_start_time
    relative_minutes = relative_time.total_minutes

    if relative_time < Time::Span::ZERO
      raise "Negative relative time!"
    end
    @ms_time_point = MSTimePoint.new(relative_minutes)
  end

  # Executes parse stage DataLine
  private def parse_data_line
    match = @@regex_data_line.match(@line)
    if match.nil?
      @stage = ParseStage::EndDataSeries
    else
      mass_string = match[1]
      current_string = match[2]
      mass = 0.0
      current = 0.0

      begin
        mass = mass_string.sub(',', '.').to_f64
      rescue exception
        puts("Error while parsing mass '#{mass_string}':")
        puts(exception.message)
        @stage = ParseStage::Abort
      end
      begin
        current = current_string.sub(',', '.').to_f64
      rescue exception
        puts("Error while parsing ion current '#{current_string}':")
        puts(exception.message)
        @stage = ParseStage::Abort
      end
      if @stage != ParseStage::Abort
        @ms_time_point.currents << IonCurrent.new(mass, current)
      end
    end
  end

  # Appends the current data series (`MSTimePoint`) to the `MSData` object.
  private def end_data_series
    @ms_data.time_points << @ms_time_point
  end

  # Checks if the last data series has already been stored to `MSData` object.
  # If not, it is appended to its collection.
  private def end_last_data_series
    if @ms_data.time_points.size > 0
      if @ms_data.time_points[-1] == @ms_time_point
        # DEBUG
        # puts "Last object in data store is equal to the parsed one"
      else
        # DEBUG
        # puts "Last object in data store is not equal, appending parsed one"
        end_data_series
      end
    else
      if @ms_time_point.currents.size > 0
        # DEBUG
        # puts "Data store is empty, but parsed object is not, appending"
        end_data_series
      else
        # DEBUG
        # puts "Everything is empty, garbage file (or software)"
      end
    end
  end
end # End class

# Stages of the parse process. Used by TGMSFileParser.
private enum ParseStage
  Sourcefile
  MeasurementStartTime
  TaskName
  ScanStartTime
  DataHeader
  DataLine
  EndDataSeries
  Abort
end
