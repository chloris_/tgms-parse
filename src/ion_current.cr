# Stores a single mass, ion current pair.
struct IonCurrent
  # Mass in atomic mass units
  getter mass
  # Ion current in Amperes
  getter ion_current

  # Initializes a new `IonCurrent` struct with provided data.
  def initialize(mass : Float64, ion_current : Float64)
    if mass < 0.0
      raise "Negative mass!"
    end
    @mass = mass

    if ion_current < 0.0
      raise "Ion current negative!"
    end
    @ion_current = ion_current
  end

  # Compares two `IonCurrent` structs by their masses.
  def <=>(other : IonCurrent)
    @mass <=> other.mass
  end
end
