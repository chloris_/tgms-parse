# tgms-parse

A parsing tool that extracts TG-MS data, exports it in a format that is easier
to plot, and generates Gnuplot scripts for 2D and 3D plots.


## Usage

Run the executable with the path of the TG-MS data file as argument. Parsed
and exported data with Gnuplot scripts should appear in the working directory
if parsing is successfull.


## Building

Crystal compiler is mandatory to build this application. Clone the repository
and run the command below. Executable `tgms-parse` should appear in
folder `bin`.

```bash
shards build --release --progress
```

## Supported TG-MS data file format

Below is an example of TG-MS data file that can be parsed by the application.

```text
Sourcefile	Mia TG vz.142 p4K.qmp
Exporttime	1.7.2020 10:16:07.247 

Start Time	12.19.2019 01:18:46.205 
End Time	12.19.2019 02:11:17.841 


Task Name	Scan
First Mass	1,00
Scan Width	90,00

Start Time	12.19.2019 01:18:46.205 

Mass [amu]	Ion Current [A]
1,00	1,335948e-009
2,00	6,376695e-011
3,00	1,426444e-011

Task Name	Scan
First Mass	1,00
Scan Width	90,00

Start Time	12.19.2019 01:18:55.354 

Mass [amu]	Ion Current [A]
1,00	1,342016e-009
2,00	6,253027e-011
3,00	1,360046e-011
```